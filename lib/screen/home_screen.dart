import 'package:flutter/material.dart';
import 'package:flutter_demo_todo_app/model/app_tab.dart';
import 'package:flutter_demo_todo_app/store/filter/store/filter_store.dart';
import 'package:flutter_demo_todo_app/store/stats/store/stats_store.dart';
import 'package:flutter_demo_todo_app/store/tab/store/tab_store.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_demo_todo_app/util/route_name.dart';
import 'package:flutter_demo_todo_app/widget/extra_actions.dart';
import 'package:flutter_demo_todo_app/widget/filter_button.dart';
import 'package:flutter_demo_todo_app/widget/filtered_todo.dart';
import 'package:flutter_demo_todo_app/widget/stats.dart';
import 'package:flutter_demo_todo_app/widget/tab_selector.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  TabStore tabStore;

  @override
  Widget build(BuildContext context) {
    tabStore = _getTabStore(context);
    return Scaffold(
      appBar: renderAppBar(),
      body: renderBody(),
      floatingActionButton: renderFloatingActionButton(context),
      bottomNavigationBar: renderBottomNavigationBar(),
    );
  }

  AppTab getActiveTab() => tabStore.activeTab;

  Widget renderAppBar() {
    return AppBar(
      title: Text('Todo'),
      actions: renderAppBarAction(),
    );
  }

  List<Widget> renderAppBarAction() {
    return [
      renderFilterButton(),
      ExtraActions(),
    ];
  }

  Widget renderFilterButton() {
    return Observer(
      builder: (_) => FilterButton(getActiveTab() == AppTab.todos),
    );
  }

  Widget renderBody() {
    return Observer(
      builder: (_) => getActiveTab() == AppTab.todos ? FilteredTodo() : Stats(),
    );
  }

  Widget renderFloatingActionButton(BuildContext context) {
    return FloatingActionButton(
      key: Keys.addTodoFab,
      onPressed: () {
        Navigator.pushNamed(context, RouteName.addEditRouteName);
      },
      child: Icon(Icons.add),
      tooltip: 'Add Todo',
    );
  }

  Widget renderBottomNavigationBar() {
    return Observer(
      builder: (_) => TabSelector(
        activeTab: getActiveTab(),
        onTabSelected: (tab) => tabStore.changeTab(tab),
      ),
    );
  }

  TabStore _getTabStore(BuildContext context) {
    return Provider.of<TabStore>(context);
  }
}
