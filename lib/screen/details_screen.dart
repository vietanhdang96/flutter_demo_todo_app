import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/screen/add_edit_screen.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class DetailsScreen extends StatelessWidget {
  TodoStore todoStore;
  final String id;

  DetailsScreen(this.id, {Key key}) : super(key: key ?? Keys.todoDetailsScreen);

  @override
  Widget build(BuildContext context) {
    todoStore = _getTodoStore(context);
    return Observer(
      builder: (_) {
        final todo = (todoStore.todoState as TodoLoadedState)
            .todoList
            .firstWhere((todo) => todo.id == id, orElse: () => null);
        return Scaffold(
          appBar: AppBar(
            title: Text('Todo Details'),
            actions: [
              IconButton(
                tooltip: 'Delete Todo',
                key: Keys.deleteTodoButton,
                icon: Icon(Icons.delete),
                onPressed: () {
                  _addDeleteTodoEvent(context, todo);
                  Navigator.pop(context, todo);
                },
              )
            ],
          ),
          body: todo == null
              ? Container(key: Keys.emptyDetailsContainer)
              : Padding(
                  padding: EdgeInsets.all(16.0),
                  child: ListView(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(right: 8.0),
                            child: Checkbox(
                                key: Keys.detailsScreenCheckBox,
                                value: todo.isCompleted,
                                onChanged: (_) {
                                  _addUpdateTodoEvent(
                                    context,
                                    todo.copyWith(
                                        isCompleted: !todo.isCompleted),
                                  );
                                }),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Hero(
                                  tag: '${todo.id}__heroTag',
                                  child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.only(
                                      top: 8.0,
                                      bottom: 16.0,
                                    ),
                                    child: Text(
                                      todo.task,
                                      key: Keys.detailsTodoItemTask,
                                      style:
                                          Theme.of(context).textTheme.headline,
                                    ),
                                  ),
                                ),
                                Text(
                                  todo.note,
                                  key: Keys.detailsTodoItemNote,
                                  style: Theme.of(context).textTheme.subhead,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
          floatingActionButton: FloatingActionButton(
            key: Keys.editTodoFab,
            tooltip: 'Edit Todo',
            child: Icon(Icons.edit),
            onPressed: todo == null
                ? null
                : () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return AddEditScreen(
                            key: Keys.editTodoScreen,
                            onSave: (task, note) {
                              _addUpdateTodoEvent(
                                context,
                                todo.copyWith(task: task, note: note),
                              );
                            },
                            isEditing: true,
                            todo: todo,
                          );
                        },
                      ),
                    );
                  },
          ),
        );
      },
    );
  }

  _addDeleteTodoEvent(BuildContext context, Todo todo) {
    todoStore.deleteTodo(todo);
  }

  _addUpdateTodoEvent(BuildContext context, Todo todo) {
    todoStore.updateTodo(todo);
  }

  TodoStore _getTodoStore(BuildContext context) {
    return Provider.of<TodoStore>(context);
  }
}
