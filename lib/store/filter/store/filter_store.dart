import 'package:flutter_demo_todo_app/model/filter_status.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_loading_state.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_todo_state.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:mobx/mobx.dart';

part 'filter_store.g.dart';

class FilterStore extends _FilterStore with _$FilterStore {
  FilterStore(TodoStore todoStore) : super(todoStore);
}

abstract class _FilterStore with Store {
  @observable
  FilterState filterState = FilterLoadingState();
  TodoStore todoStore;

  _FilterStore(this.todoStore) {
    this.todoStore = todoStore;
    this.filterState = _isTodoLoadedState()
        ? FilterLoadedState(_getTodoList(), FilterStatus.all)
        : FilterLoadingState();
    autorun((_) {
      if (todoStore.todoState is TodoLoadedState) {
        showFilteredTodoList();
      }
    });
  }

  @action
  Future updateFilterStatus(FilterStatus filterStatus) async {
    if (_isTodoLoadedState()) {
      filterState = FilterLoadedState(
        _getFilteredTodoList(_getTodoList(), filterStatus),
        filterStatus,
      );
    }
  }

  @action
  Future showFilteredTodoList() async {
    final filterStatus = _getActiveFilter();
    filterState = FilterLoadedState(
      _getFilteredTodoList(_getTodoList(), filterStatus),
      filterStatus,
    );
  }

  FilterStatus _getActiveFilter() {
    return todoStore.todoState is FilterLoadedState
        ? (todoStore.todoState as FilterLoadedState).activeFilter
        : FilterStatus.all;
  }

  bool _isTodoLoadedState() => todoStore.todoState is TodoLoadedState;

  List<Todo> _getTodoList() {
    return (todoStore.todoState as TodoLoadedState).todoList;
  }

  List<Todo> _getFilteredTodoList(
      List<Todo> todoList, FilterStatus filterStatus) {
    return todoList.where((todo) {
      switch (filterStatus) {
        case FilterStatus.completed:
          return todo.isCompleted;
          break;
        case FilterStatus.active:
          return !todo.isCompleted;
          break;
        default:
          return true;
          break;
      }
    }).toList();
  }
}
