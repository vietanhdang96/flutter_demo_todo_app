// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$FilterStore on _FilterStore, Store {
  final _$filterStateAtom = Atom(name: '_FilterStore.filterState');

  @override
  FilterState get filterState {
    _$filterStateAtom.context.enforceReadPolicy(_$filterStateAtom);
    _$filterStateAtom.reportObserved();
    return super.filterState;
  }

  @override
  set filterState(FilterState value) {
    _$filterStateAtom.context.conditionallyRunInAction(() {
      super.filterState = value;
      _$filterStateAtom.reportChanged();
    }, _$filterStateAtom, name: '${_$filterStateAtom.name}_set');
  }

  final _$updateFilterStatusAsyncAction = AsyncAction('updateFilterStatus');

  @override
  Future updateFilterStatus(FilterStatus filterStatus) {
    return _$updateFilterStatusAsyncAction
        .run(() => super.updateFilterStatus(filterStatus));
  }

  final _$showFilteredTodoListAsyncAction = AsyncAction('showFilteredTodoList');

  @override
  Future showFilteredTodoList() {
    return _$showFilteredTodoListAsyncAction
        .run(() => super.showFilteredTodoList());
  }
}
