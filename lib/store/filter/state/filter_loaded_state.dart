import 'package:flutter_demo_todo_app/model/filter_status.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_todo_state.dart';

class FilterLoadedState extends FilterState {
  final List<Todo> filteredTodoList;
  final FilterStatus activeFilter;

  const FilterLoadedState(
    this.filteredTodoList,
    this.activeFilter,
  );

  @override
  List<Object> get props => [filteredTodoList, activeFilter];

  @override
  String toString() {
    return 'FilterLoadedState { filteredTodoList: $filteredTodoList, activeFilter: $activeFilter }';
  }
}
