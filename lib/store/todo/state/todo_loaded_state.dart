import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_state.dart';

class TodoLoadedState extends TodoState {
  final List<Todo> todoList;

  const TodoLoadedState([this.todoList = const []]);

  @override
  List<Object> get props => [todoList];

  @override
  String toString() => 'TodosLoaded { todoList: $todoList }';
}
