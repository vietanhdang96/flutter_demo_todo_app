import 'package:flutter_demo_todo_app/database/todo_table.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loading_state.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_state.dart';
import 'package:mobx/mobx.dart';

part 'todo_store.g.dart';

class TodoStore = _TodoStore with _$TodoStore;

abstract class _TodoStore with Store {
  @observable
  TodoState todoState = TodoLoadingState();
  TodoTable _todoTable = TodoTable();

  @action
  Future addTodo(Todo todo) async {
    if (_isTodoLoadedState()) {
      final List<Todo> updatedTodoList = List.from(_getTodoList())..add(todo);
      todoState = TodoLoadedState(updatedTodoList);
      _todoTable.insertTodo(todo);
    }
  }

  @action
  Future deleteTodo(Todo deletedTodo) async {
    if (_isTodoLoadedState()) {
      final List<Todo> updatedTodoList =
          _getTodoList().where((todo) => todo.id != deletedTodo.id).toList();
      todoState = TodoLoadedState(updatedTodoList);
      _todoTable.deleteTodo(deletedTodo);
    }
  }

  @action
  Future loadTodo() async {
    final todoList = await _todoTable.queryTodos();
    todoState = TodoLoadedState(todoList);
  }

  @action
  Future updateTodo(Todo updatedTodo) async {
    if (_isTodoLoadedState()) {
      final List<Todo> updatedTodoList = _getTodoList().map((todo) {
        return todo.id == updatedTodo.id ? updatedTodo : todo;
      }).toList();
      todoState = TodoLoadedState(updatedTodoList);
      _todoTable.updateTodo(updatedTodo);
    }
  }

  @action
  Future toggleAllTodo() async {
    if (_isTodoLoadedState()) {
      final isAllTodoCompleted =
          _getTodoList().every((todo) => todo.isCompleted);
      final List<Todo> updatedTodoList = _getTodoList()
          .map((todo) => todo.copyWith(isCompleted: !isAllTodoCompleted))
          .toList();
      todoState = TodoLoadedState(updatedTodoList);
      _todoTable.toggleAllTodo(isAllTodoCompleted);
    }
  }

  @action
  Future clearAllCompletedTodo() async {
    if (_isTodoLoadedState()) {
      final List<Todo> updatedTodoList =
          _getTodoList().where((todo) => !todo.isCompleted).toList();
      todoState = TodoLoadedState(updatedTodoList);
      _todoTable.clearAllCompletedTodo();
    }
  }

  bool _isTodoLoadedState() => todoState is TodoLoadedState;

  List<Todo> _getTodoList() => (todoState as TodoLoadedState).todoList;
}
