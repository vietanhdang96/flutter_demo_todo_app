// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TodoStore on _TodoStore, Store {
  final _$todoStateAtom = Atom(name: '_TodoStore.todoState');

  @override
  TodoState get todoState {
    _$todoStateAtom.context.enforceReadPolicy(_$todoStateAtom);
    _$todoStateAtom.reportObserved();
    return super.todoState;
  }

  @override
  set todoState(TodoState value) {
    _$todoStateAtom.context.conditionallyRunInAction(() {
      super.todoState = value;
      _$todoStateAtom.reportChanged();
    }, _$todoStateAtom, name: '${_$todoStateAtom.name}_set');
  }

  final _$addTodoAsyncAction = AsyncAction('addTodo');

  @override
  Future addTodo(Todo todo) {
    return _$addTodoAsyncAction.run(() => super.addTodo(todo));
  }

  final _$deleteTodoAsyncAction = AsyncAction('deleteTodo');

  @override
  Future deleteTodo(Todo deletedTodo) {
    return _$deleteTodoAsyncAction.run(() => super.deleteTodo(deletedTodo));
  }

  final _$loadTodoAsyncAction = AsyncAction('loadTodo');

  @override
  Future loadTodo() {
    return _$loadTodoAsyncAction.run(() => super.loadTodo());
  }

  final _$updateTodoAsyncAction = AsyncAction('updateTodo');

  @override
  Future updateTodo(Todo updatedTodo) {
    return _$updateTodoAsyncAction.run(() => super.updateTodo(updatedTodo));
  }

  final _$toggleAllTodoAsyncAction = AsyncAction('toggleAllTodo');

  @override
  Future toggleAllTodo() {
    return _$toggleAllTodoAsyncAction.run(() => super.toggleAllTodo());
  }

  final _$clearAllCompletedTodoAsyncAction =
      AsyncAction('clearAllCompletedTodo');

  @override
  Future clearAllCompletedTodo() {
    return _$clearAllCompletedTodoAsyncAction
        .run(() => super.clearAllCompletedTodo());
  }
}
