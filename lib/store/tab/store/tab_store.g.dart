// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tab_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TabStore on _TabStore, Store {
  final _$activeTabAtom = Atom(name: '_TabStore.activeTab');

  @override
  AppTab get activeTab {
    _$activeTabAtom.context.enforceReadPolicy(_$activeTabAtom);
    _$activeTabAtom.reportObserved();
    return super.activeTab;
  }

  @override
  set activeTab(AppTab value) {
    _$activeTabAtom.context.conditionallyRunInAction(() {
      super.activeTab = value;
      _$activeTabAtom.reportChanged();
    }, _$activeTabAtom, name: '${_$activeTabAtom.name}_set');
  }

  final _$changeTabAsyncAction = AsyncAction('changeTab');

  @override
  Future changeTab(AppTab appTab) {
    return _$changeTabAsyncAction.run(() => super.changeTab(appTab));
  }
}
