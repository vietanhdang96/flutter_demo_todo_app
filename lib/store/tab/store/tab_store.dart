import 'package:flutter_demo_todo_app/model/app_tab.dart';
import 'package:mobx/mobx.dart';

part 'tab_store.g.dart';

class TabStore = _TabStore with _$TabStore;

abstract class _TabStore with Store {
  @observable
  AppTab activeTab = AppTab.todos;

  @action
  Future changeTab(AppTab appTab) async {
    this.activeTab = appTab;
  }
}
