import 'package:flutter_demo_todo_app/store/stats/state/stats_state.dart';

class StatsLoadedState extends StatsState {
  final int activeNumber;
  final int completedNumber;

  const StatsLoadedState(this.activeNumber, this.completedNumber);

  @override
  List<Object> get props => [activeNumber, completedNumber];

  @override
  String toString() {
    return 'StatsLoaded { numActive: $activeNumber, numCompleted: $completedNumber }';
  }
}
