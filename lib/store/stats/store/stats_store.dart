import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/store/stats/state/stats_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/stats/state/stats_loading_state.dart';
import 'package:flutter_demo_todo_app/store/stats/state/stats_state.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:mobx/mobx.dart';

part 'stats_store.g.dart';

class StatsStore extends _StatsStore with _$StatsStore {
  StatsStore(TodoStore todoStore) : super(todoStore);
}

abstract class _StatsStore with Store {
  @observable
  StatsState statsState = StatsLoadingState();
  TodoStore todoStore;

  _StatsStore(this.todoStore) {
    this.todoStore = todoStore;
    autorun((_) {
      if (todoStore.todoState is TodoLoadedState) {
        updateStats(_getTodoList());
      }
    });
  }

  @action
  Future updateStats(List<Todo> todoList) async {
    int activeNumber =
        todoList.where((todo) => !todo.isCompleted).toList().length;
    int completedNumber =
        todoList.where((todo) => todo.isCompleted).toList().length;
    statsState = StatsLoadedState(activeNumber, completedNumber);
  }

  bool _isTodoLoadedState() => todoStore.todoState is TodoLoadedState;

  List<Todo> _getTodoList() {
    return (todoStore.todoState as TodoLoadedState).todoList;
  }
}
