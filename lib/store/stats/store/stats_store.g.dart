// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stats_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$StatsStore on _StatsStore, Store {
  final _$statsStateAtom = Atom(name: '_StatsStore.statsState');

  @override
  StatsState get statsState {
    _$statsStateAtom.context.enforceReadPolicy(_$statsStateAtom);
    _$statsStateAtom.reportObserved();
    return super.statsState;
  }

  @override
  set statsState(StatsState value) {
    _$statsStateAtom.context.conditionallyRunInAction(() {
      super.statsState = value;
      _$statsStateAtom.reportChanged();
    }, _$statsStateAtom, name: '${_$statsStateAtom.name}_set');
  }

  final _$updateStatsAsyncAction = AsyncAction('updateStats');

  @override
  Future updateStats(List<Todo> todoList) {
    return _$updateStatsAsyncAction.run(() => super.updateStats(todoList));
  }
}
