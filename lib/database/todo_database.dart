import 'package:flutter_demo_todo_app/database/todo_table.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class TodoDatabase {
  static const DB_NAME = 'todo.db';
  static const DB_VERSION = 1;
  static Database _database;
  static const initScript = [TodoTable.CREATE_TABLE_QUERY];
  static const migrationScript = [TodoTable.CREATE_TABLE_QUERY];

  TodoDatabase._internal();

  Database get database => _database;

  static final TodoDatabase instance = TodoDatabase._internal();

  init() async {
    _database = await openDatabase(
      join(await getDatabasesPath(), DB_NAME),
      onCreate: (db, version) async {
        await _executeScript(db, initScript);
      },
      onUpgrade: (db, oldVersion, newVersion) async {
        await Future.forEach(migrationScript, (script) async {
          await db.execute(script);
        });
      },
      version: DB_VERSION,
    );
  }

  _executeScript(Database database, List<String> scriptList) async {
    await Future.forEach(scriptList, (script) async {
      await database.execute(script);
    });
  }
}
