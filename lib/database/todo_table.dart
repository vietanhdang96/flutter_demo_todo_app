import 'package:flutter_demo_todo_app/database/todo_database.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:sqflite/sqflite.dart';

class TodoTable {
  static const TABLE_NAME = 'todo';
  static const CREATE_TABLE_QUERY = '''
    CREATE TABLE $TABLE_NAME(
      id TEXT PRIMARY KEY,
      task TEXT,
      note TEXT,
      isCompleted INTEGER
    )
  ''';
  static const DROP_TABLE_QUERY = '''
    DROP TABLE IF EXIST $TABLE_NAME
  ''';
  static const COLUMN_ID = 'id';
  static const COLUMN_TASK = 'task';
  static const COLUMN_NOTE = 'note';
  static const COLUMN_ISCOMPLETED = 'isCompleted';
  static const INSERT_CONFLICT_ALGORITHM = ConflictAlgorithm.replace;

  Database get database => TodoDatabase.instance.database;

  Future<int> insertTodo(Todo todo) async {
    return _insert(todo);
  }

  Future<int> deleteTodo(Todo todo) async {
    return _delete(
      '$COLUMN_ID = ?',
      [todo.id],
    );
  }

  Future<int> updateTodo(Todo todo) async {
    return _update(
      todo,
      '$COLUMN_ID = ?',
      [todo.id],
    );
  }

  Future<int> toggleAllTodo(bool isAllTodoCompleted) async {
    return _rawUpdate(
      COLUMN_ISCOMPLETED,
      isAllTodoCompleted ? [0, 1] : [1, 0],
    );
  }

  Future<int> clearAllCompletedTodo() async {
    return _delete(
      '$COLUMN_ISCOMPLETED = ?',
      [1],
    );
  }

  Future<List<Todo>> queryTodos() async {
    final List<Map<String, dynamic>> todoMaps =
        await database.query(TABLE_NAME);
    return List.generate(todoMaps.length, (index) {
      var todo = todoMaps[index];
      return Todo.fromData(
        todo[COLUMN_ID],
        todo[COLUMN_TASK],
        todo[COLUMN_NOTE],
        todo[COLUMN_ISCOMPLETED],
      );
    });
  }

  _insert(Todo todo) {
    database.insert(
      TABLE_NAME,
      todo.toMap(),
      conflictAlgorithm: INSERT_CONFLICT_ALGORITHM,
    );
  }

  _delete(String where, List<dynamic> whereArgs) {
    database.delete(
      TABLE_NAME,
      where: where,
      whereArgs: whereArgs,
    );
  }

  _update(Todo todo, String where, List<dynamic> whereArgs) {
    database.update(
      TABLE_NAME,
      todo.toMap(),
      where: '$COLUMN_ID = ?',
      whereArgs: [todo.id],
    );
  }

  _rawUpdate(String column, List<dynamic> arguments) {
    database.rawUpdate(
      'UPDATE $TABLE_NAME SET $column = ? WHERE $column = ?',
      arguments,
    );
  }
}
