import 'package:flutter/material.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';

class DeleteTodoSnackBar extends SnackBar {
  DeleteTodoSnackBar({
    Key key,
    @required Todo todo,
    @required VoidCallback onUndo,
  }) : super(
          key: key,
          content: Text(
            todo.task + " deleted",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label: "Undo",
            onPressed: onUndo,
          ),
        );
}
