import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/screen/details_screen.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_loading_state.dart';
import 'package:flutter_demo_todo_app/store/filter/store/filter_store.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_demo_todo_app/widget/delete_todo_snack_bar.dart';
import 'package:flutter_demo_todo_app/widget/loading_indicator.dart';
import 'package:flutter_demo_todo_app/widget/todo_item.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class FilteredTodo extends StatelessWidget {
  FilterStore filterStore;
  TodoStore todoStore;

  FilteredTodo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    todoStore = _getTodoStore(context);
    filterStore = _getFilterStore(context);
    return Observer(
      builder: (_) {
        if (filterStore.filterState is FilterLoadingState) {
          return LoadingIndicator(key: Keys.todosLoading);
        } else if (filterStore.filterState is FilterLoadedState) {
          final todoList =
              (filterStore.filterState as FilterLoadedState).filteredTodoList;
          return ListView.builder(
            key: Keys.todoList,
            itemCount: todoList.length,
            itemBuilder: (BuildContext context, int index) {
              final todo = todoList[index];
              return TodoItem(
                todo: todo,
                onDismissed: (direction) {
                  _deleteTodo(todo);
                  Scaffold.of(context).showSnackBar(DeleteTodoSnackBar(
                    key: Keys.snackbar,
                    todo: todo,
                    onUndo: () => _addTodo(todo),
                  ));
                },
                onTap: () async {
                  final removedTodo = await Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => DetailsScreen(todo.id),
                    ),
                  );
                  if (removedTodo != null) {
                    Scaffold.of(context).showSnackBar(DeleteTodoSnackBar(
                      key: Keys.snackbar,
                      todo: todo,
                      onUndo: () => _addTodo(todo),
                    ));
                  }
                },
                onCheckboxChanged: (isCompletedTodo) {
                  _updateTodo(todo, isCompletedTodo);
                },
              );
            },
          );
        } else {
          return Container(key: Keys.filteredTodosEmptyContainer);
        }
      },
    );
  }

  _deleteTodo(Todo todo) {
    todoStore.deleteTodo(todo);
  }

  _addTodo(Todo todo) {
    todoStore.addTodo(todo);
  }

  _updateTodo(Todo todo, bool isCompletedTodo) {
    todoStore.updateTodo(todo.copyWith(isCompleted: isCompletedTodo));
  }

  TodoStore _getTodoStore(BuildContext context) {
    return Provider.of<TodoStore>(context);
  }

  FilterStore _getFilterStore(BuildContext context) {
    return Provider.of<FilterStore>(context);
  }
}
