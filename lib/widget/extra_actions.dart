import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_demo_todo_app/model/extra_action.dart';
import 'package:flutter_demo_todo_app/store/todo/state/todo_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class ExtraActions extends StatelessWidget {
  TodoStore todoStore;

  @override
  Widget build(BuildContext context) {
    todoStore = _getTodoStore(context);
    return Observer(
      builder: (_) {
        if (todoStore.todoState is TodoLoadedState) {
          bool allComplete = (todoStore.todoState as TodoLoadedState)
              .todoList
              .every((todo) => todo.isCompleted);
          return PopupMenuButton<ExtraAction>(
            key: Keys.extraActionsPopupMenuButton,
            onSelected: (action) {
              switch (action) {
                case ExtraAction.clearCompleted:
                  _clearAllCompletedTodo();
                  break;
                case ExtraAction.toggleAllComplete:
                  _toggleAllTodo();
                  break;
              }
            },
            itemBuilder: (BuildContext context) => <PopupMenuItem<ExtraAction>>[
              PopupMenuItem<ExtraAction>(
                key: Key('toggleAllComplete'),
                value: ExtraAction.toggleAllComplete,
                child: Text(
                  allComplete ? 'Mark All Incomplete' : 'Mark All Complete',
                ),
              ),
              PopupMenuItem<ExtraAction>(
                key: Key('clearCompleted'),
                value: ExtraAction.clearCompleted,
                child: Text(
                  'Clear Completed',
                ),
              ),
            ],
          );
        }
        return Container(key: Keys.extraActionsEmptyContainer);
      },
    );
  }

  _clearAllCompletedTodo() {
    todoStore.clearAllCompletedTodo();
  }

  _toggleAllTodo() {
    todoStore.toggleAllTodo();
  }

  TodoStore _getTodoStore(BuildContext context) {
    return Provider.of<TodoStore>(context);
  }
}
