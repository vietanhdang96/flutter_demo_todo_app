import 'package:flutter/material.dart';
import 'package:flutter_demo_todo_app/model/filter_status.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/filter/state/filter_todo_state.dart';
import 'package:flutter_demo_todo_app/store/filter/store/filter_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class FilterButton extends StatelessWidget {
  final bool visible;
  FilterStore filterStore;

  FilterButton(this.visible, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final defaultStyle = Theme.of(context).textTheme.body1;
    final activeStyle =
        defaultStyle.copyWith(color: Theme.of(context).accentColor);
    filterStore = _getFilterStore(context);
    return Observer(builder: (_) {
      final button = _Button(
        onSelected: (filter) => _updateFilterStatus(filter),
        activeFilter: _getActiveFilter(filterStore.filterState),
        activeStyle: activeStyle,
        defaultStyle: defaultStyle,
      );
      return AnimatedOpacity(
        opacity: visible ? 1.0 : 0.0,
        duration: Duration(milliseconds: 150),
        child: visible ? button : IgnorePointer(child: button),
      );
    });
  }

  FilterStatus _getActiveFilter(FilterState filterState) {
    return filterState is FilterLoadedState
        ? filterState.activeFilter
        : FilterStatus.all;
  }

  _updateFilterStatus(FilterStatus filterStatus) {
    filterStore.updateFilterStatus(filterStatus);
  }

  FilterStore _getFilterStore(BuildContext context) {
    return Provider.of<FilterStore>(context);
  }
}

class _Button extends StatelessWidget {
  const _Button({
    Key key,
    @required this.onSelected,
    @required this.activeFilter,
    @required this.activeStyle,
    @required this.defaultStyle,
  }) : super(key: key);

  final PopupMenuItemSelected<FilterStatus> onSelected;
  final FilterStatus activeFilter;
  final TextStyle activeStyle;
  final TextStyle defaultStyle;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<FilterStatus>(
      key: Keys.filterButton,
      tooltip: 'Filter Todos',
      onSelected: onSelected,
      itemBuilder: (BuildContext context) => <PopupMenuItem<FilterStatus>>[
        PopupMenuItem<FilterStatus>(
          key: Keys.allFilter,
          value: FilterStatus.all,
          child: Text(
            'Show All',
            style:
                activeFilter == FilterStatus.all ? activeStyle : defaultStyle,
          ),
        ),
        PopupMenuItem<FilterStatus>(
          key: Keys.activeFilter,
          value: FilterStatus.active,
          child: Text(
            'Show Active',
            style: activeFilter == FilterStatus.active
                ? activeStyle
                : defaultStyle,
          ),
        ),
        PopupMenuItem<FilterStatus>(
          key: Keys.completedFilter,
          value: FilterStatus.completed,
          child: Text(
            'Show Completed',
            style: activeFilter == FilterStatus.completed
                ? activeStyle
                : defaultStyle,
          ),
        ),
      ],
      icon: Icon(Icons.filter_list),
    );
  }
}
