import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_demo_todo_app/store/stats/state/stats_loaded_state.dart';
import 'package:flutter_demo_todo_app/store/stats/state/stats_loading_state.dart';
import 'package:flutter_demo_todo_app/store/stats/store/stats_store.dart';
import 'package:flutter_demo_todo_app/util/keys.dart';
import 'package:flutter_demo_todo_app/widget/loading_indicator.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class Stats extends StatelessWidget {
  StatsStore statsStore;

  Stats({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    statsStore = _getStatsStore(context);
    return Observer(
      builder: (_) {
        if (statsStore.statsState is StatsLoadingState) {
          return LoadingIndicator(key: Keys.statsLoadingIndicator);
        } else if (statsStore.statsState is StatsLoadedState) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Completed Todos',
                    style: Theme.of(context).textTheme.title,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    "${(statsStore.statsState as StatsLoadedState).completedNumber}",
                    key: Keys.statsNumCompleted,
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Active Todos',
                    style: Theme.of(context).textTheme.title,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    "${(statsStore.statsState as StatsLoadedState).activeNumber}",
                    key: Keys.statsNumActive,
                    style: Theme.of(context).textTheme.subhead,
                  ),
                )
              ],
            ),
          );
        } else {
          return Container(key: Keys.emptyStatsContainer);
        }
      },
    );
  }

  StatsStore _getStatsStore(BuildContext context) {
    return Provider.of<StatsStore>(context);
  }
}
