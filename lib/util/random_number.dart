import 'dart:math';

class RandomNumber {
  static var _randomInt = Random();

  static int getRandomNumber() => _randomInt.nextInt(1000000);
}
