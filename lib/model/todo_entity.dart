class TodoEntity {
  final bool isCompleted;
  final String id;
  final String note;
  final String task;

  TodoEntity(this.task, this.id, this.note, this.isCompleted);

  @override
  int get hashCode =>
      isCompleted.hashCode ^ task.hashCode ^ note.hashCode ^ id.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TodoEntity &&
          runtimeType == other.runtimeType &&
          isCompleted == other.isCompleted &&
          task == other.task &&
          note == other.note &&
          id == other.id;

  Map<String, Object> toJson() {
    return {
      "isCompleted": isCompleted,
      "task": task,
      "note": note,
      "id": id,
    };
  }

  @override
  String toString() {
    return 'TodoEntity{isCompleted: $isCompleted, task: $task, note: $note, id: $id}';
  }

  static TodoEntity fromJson(Map<String, Object> json) {
    return TodoEntity(
      json["task"] as String,
      json["id"] as String,
      json["note"] as String,
      json["isCompleted"] as bool,
    );
  }
}
