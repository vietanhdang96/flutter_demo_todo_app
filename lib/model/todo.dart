import 'package:equatable/equatable.dart';
import 'package:flutter_demo_todo_app/model/todo_entity.dart';
import 'package:flutter_demo_todo_app/util/uuid.dart';

class Todo extends Equatable {
  String id;
  String task;
  String note;
  bool isCompleted;

  Todo.fromData(this.id, this.task, this.note, int isCompleted)
      : this.isCompleted = isCompleted == 0 ? false : true;

  Todo(this.task, {this.isCompleted = false, String note = '', String id})
      : this.note = note ?? '',
        this.id = id ?? Uuid().generateV4();

  Todo copyWith({bool isCompleted, String id, String note, String task}) {
    return Todo(
      task ?? this.task,
      isCompleted: isCompleted ?? this.isCompleted,
      id: id ?? this.id,
      note: note ?? this.note,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'task': task,
      'note': note,
      'isCompleted': isCompleted ? 1 : 0,
    };
  }

  TodoEntity toEntity() {
    return TodoEntity(task, id, note, isCompleted);
  }

  static Todo fromEntity(TodoEntity entity) {
    return Todo(
      entity.task,
      isCompleted: entity.isCompleted ?? false,
      note: entity.note,
      id: entity.id ?? Uuid().generateV4(),
    );
  }

  @override
  List<Object> get props => [id, task, note, isCompleted];

  @override
  String toString() {
    return 'Todo { complete: $isCompleted, task: $task, note: $note, id: $id }';
  }
}
