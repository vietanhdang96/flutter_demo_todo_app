import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_demo_todo_app/base/base_bloc_delegate.dart';
import 'package:flutter_demo_todo_app/database/todo_database.dart';
import 'package:flutter_demo_todo_app/model/todo.dart';
import 'package:flutter_demo_todo_app/screen/add_edit_screen.dart';
import 'package:flutter_demo_todo_app/screen/home_screen.dart';
import 'package:flutter_demo_todo_app/store/filter/store/filter_store.dart';
import 'package:flutter_demo_todo_app/store/stats/store/stats_store.dart';
import 'package:flutter_demo_todo_app/store/tab/store/tab_store.dart';
import 'package:flutter_demo_todo_app/store/todo/store/todo_store.dart';
import 'package:flutter_demo_todo_app/util/route_name.dart';
import 'package:provider/provider.dart';

void main() async {
  BlocSupervisor.delegate = BaseBlocDelegate();
  await TodoDatabase.instance.init();
  runApp(
    Provider(
      create: (context) => TodoStore()..loadTodo(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  final TabStore tabStore = TabStore();

  @override
  Widget build(BuildContext context) {
    TodoStore todoStore = _getTodoStore(context);
    FilterStore filterStore = FilterStore(todoStore);
    StatsStore statsStore = StatsStore(todoStore);

    return MaterialApp(
      title: 'Todos',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        RouteName.homeRouteName: (context) {
          return MultiProvider(
            providers: [
              Provider<FilterStore>(create: (context) => filterStore),
              Provider<StatsStore>(create: (context) => statsStore),
              Provider<TabStore>(create: (context) => tabStore),
            ],
            child: HomeScreen(),
          );
        },
        RouteName.addEditRouteName: (context) {
          return AddEditScreen(
            key: Key('__addTodoScreen__'),
            onSave: (task, note) {
              _addAddTodoEvent(context, Todo(task, note: note));
            },
            isEditing: false,
          );
        },
      },
    );
  }

  TodoStore _getTodoStore(BuildContext context) {
    return Provider.of<TodoStore>(context);
  }

  _addAddTodoEvent(BuildContext context, Todo todo) {
    _getTodoStore(context).addTodo(todo);
  }
}
